#include<stdio.h>

void insertionSort(int *array, int length);
void insert(int *array, int length, int num);

int main() {

    int array[] = { 6, 5, 4, 3, 8, 9, 0, 2, 1 };
    int length = 9;
    int flag = 0;

    insertionSort(array, length);
    
    int j;
    for (j = 0; j < 9; j++) {
        printf("%d ", array[j]);
    }

    printf("\n");

}

void insertionSort(int *array, int length) {
    if (length > 0) {
        insertionSort(array, length - 1);
        insert(array, length, array[length]);
    }
}

void insert(int *array, int length, int num) {
    int i;
    for (i = length - 1; i > 0; i--) {
        if (array[i] < array[i - 1]) {
            int temp = array[i];
            array[i] = array[i - 1];
            array[i - 1] = temp;
        } else {
            break;
        }
        
    }
}