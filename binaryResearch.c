#include<stdio.h>

int binaryResearch(int *array, int posStart, int posEnd, int key);

int main() {

    int array[30000];
    int i;
    for (i = 0; i < 30000; i++) {
        array[i] = i;
    }

    (binaryResearch(array, 0, 29999, 292132)) ? printf("ok\n") : printf("ko\n");

    return 0;
}
int binaryResearch(int *array, int posStart, int posEnd, int key) {
    int len = posEnd - posStart + 1;
    if (len < 1) {
        return (array[posStart] == key) ? 1 : 0;
    } else {
        len = (len / 2) + posStart;
        if (array[len] < key) {
            binaryResearch(array, len + 1, posEnd, key);
        } else if (array[len] > key) {
            binaryResearch(array, posStart, len - 1, key);
        } else {
            return 1;
        }
    }
    
}