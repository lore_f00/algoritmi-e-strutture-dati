#include<stdio.h>
#include<stdlib.h>

void mergeSortForSearchRepetition(long *array, int p, int r, long *repeted, int *find);
void merge(long *array, int start, int center, int end, long *repeted, int *find);
void copyArray(long *array, int *copy, int start, int end);

int main(int argc, char **argvs) {

    int size = argc - 1;

    // array le cui posizioni partono da 1
    long array[size];
    long repeted;
    int find = 0;

    int i = 0;
    for (i = 0; i < size; i++) {;
        array[i] = atoi(argvs[i + 1]);
    }

    mergeSortForSearchRepetition(array, 1, size, &repeted, &find);

    (find) ? printf("numero ripetuto: %ld\n", repeted) : printf("nessuna ripetizione\n");

    return 0;
}

void mergeSortForSearchRepetition(long *array, int p, int r, long *repeted, int *find) {
    if (!*find && p < r) {
        int q = (p + r) / 2;
        mergeSortForSearchRepetition(array, p, q, repeted, find);
        mergeSortForSearchRepetition(array, q + 1, r, repeted, find);
        merge(array, p, q, r, repeted, find);
    }
}

void merge(long *array, int start, int center, int end, long *repeted, int *find) {
    int i = start;
    int c = center + 1;
    int f = end;
    int copy[end - start + 1];

    int k = 1;
    while (!*find && (i <= center || c <= end)) {
        if (i <= center && (c > end || array[i] <= array[c])) {
            if (array[i] == array[c]) {
                *repeted = array[i];
                *find = 1;
            } else {
                copy[k] = array[i];
                i++;
            }
        } else {
            copy[k] = array[c];
            c++;
        }
        k++;
    }
    if (!*find) {
        copyArray(array, copy, start, end);
    }
}

void copyArray(long *array, int *copy, int start, int end) {
    int k;
    for (k = start; k <= end; k++) {
        array[k] = copy[k - start + 1];
    }
}