#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<math.h> 

void insertionSort(int *array);
void insertionSortBin(int *array);
void bubbleSort(int *array);
int binaryResearchSort(int *array, int posStart, int posEnd, int key);
int binaryResearch(int *array, int posStart, int posEnd, int key);

int main() {

    int array[30000];
    int i;
    for (i = 0; i < 30000; i++) {
        array[i] = rand() % 30000;
    }
    int array1[30000];
    int array2[30000];
    int array3[30000];

    for (i = 0; i < 30000; i++) {
        array1[i] = array[i];
        array2[i] = array[i];
        array3[i] = array[i];
    }

    insertionSort(array1);
    insertionSortBin(array2);
    bubbleSort(array3);

    //(binaryResearch(array, 0, 4, 6)) ? printf("ok\n") : printf("ko\n");

    return 0;
}

void bubbleSort(int *array) {
    clock_t start = clock();

    int i, j;
    for (i = 0; i < 30000; i++) {
        for (j = 29999; j > i; j--) {
            if (array[j] < array[j - 1]) {
                int temp = array[j];
                array[j] = array[j - 1];
                array[j - 1] = temp;
            }
        }
    }

    clock_t end = clock();
    printf("Tempo di esecuzione =  %f secondi \n", ((double)(end - start)) / CLOCKS_PER_SEC);
}


void insertionSort(int *array) {
    clock_t start = clock();

    int j;
    for (j = 1; j < 30000; j++) {
        int key = array[j];
        int i = j - 1;
        while (i >= 0 && array[i] > key) {
            array[i + 1] = array[i];
            i--;
        }
        array[i + 1] = key;
    }

    clock_t end = clock();
    printf("Tempo di esecuzione =  %f secondi \n", ((double)(end - start)) / CLOCKS_PER_SEC);

    printf("\n");
}


void insertionSortBin(int *array) {
    clock_t start = clock();

    int j;
    for (j = 1; j < 30000; j++) {
        int key = array[j];
        int i = j - 1;
        int pos = binaryResearchSort(array, 0, i, key);
        while (i >= pos) {
            array[i + 1] = array[i];
            i--;
        }
        array[i + 1] = key;
    }

    clock_t end = clock();
    printf("Tempo di esecuzione =  %f secondi \n", ((double)(end - start)) / CLOCKS_PER_SEC);

    printf("\n");
}

int binaryResearchSort(int *array, int posStart, int posEnd, int key) {
    int len = posEnd - posStart + 1;
    if (len < 1) {
        return (array[posStart] < key) ? posStart + 1 : posStart;
    } else {
        len = (len / 2) + posStart;
        if (array[len] < key) {
            return binaryResearchSort(array, len + 1, posEnd, key);
        } else if (array[len] > key) {
            return binaryResearchSort(array, posStart, len - 1, key);
        } else {
            return len;
        }
    }
    
}

int binaryResearch(int *array, int posStart, int posEnd, int key) {
    int len = posEnd - posStart + 1;
    if (len == 1) {
        return (array[posStart] == key) ? 1 : 0;
    } else {
        len = (len / 2) + posStart;
        if (array[len] < key) {
            binaryResearch(array, len + 1, posEnd, key);
        } else if (array[len] > key) {
            binaryResearch(array, posStart, len - 1, key);
        } else {
            return 1;
        }
    }
    
}