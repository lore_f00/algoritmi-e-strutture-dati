#include<stdio.h>

void mergeSort(int *array, int p, int r);
void merge(int *array, int start, int center, int end);
void alternativeMergeSort(int *array, int p, int r);
void alternativeMerge(int *array, int p, int q, int r);
void copyArray(int *array, int *copy, int start, int end);

int main() {

    int array[] = { 0, 7, 8, 6, 11, 2, 1 };
    int j = 2;
    mergeSort(array, 1, 6);

    for (j = 1; j < 7; j++) {
        printf("%d ", array[j]);
    }

    printf("\n");

}

void mergeSort(int *array, int p, int r) {
    if (p < r) {
        int q = (p + r) / 2;
        mergeSort(array, p, q);
        mergeSort(array, q + 1, r);
        merge(array, p, q, r);
    }
}

void merge(int *array, int start, int center, int end) {
    int i = start;
    int c = center + 1;
    int f = end;
    int copy[end - start + 1];

    int k = 1;
    while (i <= center || c <= end) {
        if (i <= center && (c > end || array[i] <= array[c])) {
            copy[k] = array[i];
            i++;
        } else {
            copy[k] = array[c];
            c++;
        }
        k++;
    }
    copyArray(array, copy, start, end);
}

void alternativeMergeSort(int *array, int p, int r) {
    if (p < r) {
        int q = (p + r) / 2;
        mergeSort(array, p, q);
        mergeSort(array, q + 1, r);
        alternativeMerge(array, p, q, r);
    }
}

void alternativeMerge(int *array, int start, int center, int end) {
    int i = start;
    int c = center + 1;
    int f = end;
    int copy[end - start + 1];

    int k = 1;
    while (i <= center && c <= end) {
        if (array[i] <= array[c]) {
            copy[k] = array[i];
            i++;
        } else {
            copy[k] = array[c];
            c++;
        }
        k++;
    }
    while (i <= center) {
        copy[k] = array[i];
        i++;
        k++;
    }
    while (c <= end) {
        copy[k] = array[c];
        c++;
        k++;
    }
    copyArray(array, copy, start, end);
}

void copyArray(int *array, int *copy, int start, int end) {
    int k;
    for (k = start; k <= end; k++) {
        array[k] = copy[k - start + 1];
    }
}