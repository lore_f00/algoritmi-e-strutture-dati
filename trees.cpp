#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <fstream>
#include <vector>
#include <cstdio>

using namespace std;

class BSTNode {

    private:

        int value;
        BSTNode *left; 
        BSTNode *rigth;

    public:

        BSTNode *insertNode(BSTNode *node) {
            if (node->getValue() < this->value) {
                if (this->left != NULL) {
                    this->left = this->left->insertNode(node);
                } else {
                    this->left = node;
                }
            } else if (node->getValue() > this->value) {
                if (this->rigth != NULL) {
                    this->rigth = this->rigth->insertNode(node);
                } else {
                    this->rigth = node;
                }
            }
        }

        bool findNode(int value) {
            if (value < this->value) {
                return this->left != NULL ? this->left->findNode(value) : false;
            } else if (value > this->value) {
                return this->rigth != NULL ? this->rigth->findNode(value) : false;
            } else {
                return true;
            }
        }

        BSTNode(int value) {
            this->value = value;
        }
        
        BSTNode *getLeft() {
            return this->left;
        }

        BSTNode *setLeft(BSTNode *left) {
            this->left = left;
            return this;
        }

        BSTNode *getRigth() {
            return this->rigth;
        }

        BSTNode *setRigth(BSTNode *rigth) {
            this->rigth = rigth;
            return this;
        }

        int getValue() {
            return this->value;
        }
};

class RBTNode {

    const string COLOR_RED = "red";
    const string COLOR_BLACK = "black";

    private:

        int value;
        string color = COLOR_RED;
        RBTNode *left; 
        RBTNode *rigth;

    public:

        RBTNode(int value) {
            this->value = value;
        }
        
        RBTNode *getLeft() {
            return this->left;
        }

        void setLeft(RBTNode *left) {
            this->left = left;
        }

        RBTNode *getRigth() {
            return this->rigth;
        }

        RBTNode *setRigth(RBTNode *rigth) {
            this->rigth = rigth;
            return this;
        }

        RBTNode *getRigth() {
            return this->rigth;
        }

        RBTNode *setRigth(RBTNode *rigth) {
            this->rigth = rigth;
            return this;
        }

        string getColor() {
            return this->color;
        }

        RBTNode *setColor(string color) {
            this->color = color;
            return this;
        }

        int getValue() {
            return this->value;
        }
};

int main () {
    BSTNode root = BSTNode(1);
    for (int i = 0; i < 100000; i++) {
        root.insertNode(&BSTNode(rand() % 100000));
    }
}