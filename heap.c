#include<stdio.h>
#include<stdlib.h>

int left(int i) {
    return 2 * i;
}


int right(int i) {
    return 2 * i + 1;
}

int parent(int i) {
    return i / 2;
}

void swap(int *array, int start, int end) {
    long temp = array[start];
    array[start] = array[end];
    array[end] = temp;
}

int compareValues(int val1, int val2, int *isMinHeap) {
    if (*isMinHeap) {
        return val1 < val2;
    } else {
        return val1 > val2;
    }
}

void heapInsert(int *heap, int k, int heapSize, int *arraySize, int isMinHeap) {
    if (heapSize < *arraySize) {
        heapSize++;
        heap[heapSize] = k;
        int i = heapSize;
        while (i > 0 && parent(i) > 0 && compareValues(heap[i], heap[parent(i)], &isMinHeap)) {
            swap(heap, i, parent(i));
            i = parent(i);
        }
    }
}

void heapify(int *heap, int heapSize, int i, int *isMinHeap) {
    int l = left(i);
    int r = right(i);

    int m;
    if (l <= heapSize && compareValues(heap[l], heap[i], isMinHeap)) {
        m = l;
    } else {
        m = i;
    }
    if (r <= heapSize && compareValues(heap[r], heap[m], isMinHeap)) {
        m = r;
    }
    if (m != i) {
        swap(heap, i, m);
        heapify(heap, heapSize, m, isMinHeap);
    }
}

void heapExtract(int *heap, int heapSize, int *isMinHeap) {
    if (heapSize >= 1) {
        swap(heap, heapSize, 1);
        heapSize--;
        heapify(heap, heapSize, 1, isMinHeap);
    }
}

void heapSort(int *heap, int heapSize, int isMinHeap) {
    int i = 1;
    int end = heapSize;
    for (int i = 1; i < end; i++) {
        heapExtract(heap, heapSize, &isMinHeap);
        heapSize--;
    }
}

void printHeap(int *heap, int *heapSize) {
    int i = 1;
    for (i = 1; i <= *heapSize; i++) {;
        printf("%d\t", heap[i]);
    } 
    printf("\n");
}

int main (int argc, char **argvs) {
    int maxHeap[argc];
    int minHeap[argc];
    int maxHeapSize = 0;
    int minHeapSize = 0;

    int arraySize = argc;

    // prelevo valori da argvs
    int i = 1;
    for (i = 1; i < arraySize; i++) {
        // riempio la minHeap
        heapInsert(minHeap, atoi(argvs[i]), minHeapSize, &arraySize, 1);
        minHeapSize++;

        // riempio la minHeap
        heapInsert(maxHeap, atoi(argvs[i]), maxHeapSize, &arraySize, 0);
        maxHeapSize++;
    }

    // stampo la maxHeap
    printf("maxHeap: ");
    printHeap(maxHeap, &maxHeapSize);

    // stampo la minHeap
    printf("\nminHeap: ");
    printHeap(minHeap, &minHeapSize);

    heapSort(maxHeap, maxHeapSize, 0);

    heapSort(minHeap, minHeapSize, 1);

    // stampo la maxHeap ordinata in ordine decrescente
    printf("\nmaxHeap ordinata: ");
    printHeap(maxHeap, &maxHeapSize);

    // stampo la minHeap ordinata in ordine crescente
    printf("\nminHeap ordinata: ");
    printHeap(minHeap, &minHeapSize);

    return 0;
}