#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<math.h> 

int majorityBetter(int *array, int size, int *majority);
int majorityWorst(int *array, int size, int *majority);

int main() {

    int array[50000];
    int i;
    int size = rand() % 50000;
    for (i = 0; i < size; i++) {
        array[i] = rand() % size;
    }

    printf("size = %d\n", size);

    //int array1[] = { 5, 0, 3, 1, 13, 7, 5 };
    //int array2[] = { 5, 0, 3, 0, 5, 0, 0 };

    int majority;
    (majorityBetter(array, size, &majority)) ? printf("%d\n", majority) : printf("no majority\n");
    int majority2;
    (majorityWorst(array, size, &majority2)) ? printf("%d\n", majority2) : printf("no majority\n");

    return 0;
}

int majorityBetter(int *array, int size, int *majority) {
    int max = 0, i;
    clock_t start = clock();

    for (i = 0; i < size; i++) {
        if (max < array[i]) {
            max = array[i];
        }
    }

    int array_el[max + 1];
    for (i = 0; i <= max; i++) {
        array_el[i] = 0;
    }

    for (i = 0; i < size; i++) {
        array_el[array[i]]++;
        if (array_el[array[i]] >= (size / 2) + 1) {
            *majority = array[i];
            break;
        }
    }

    clock_t end = clock();
    printf("Tempo di esecuzione =  %f secondi \n", ((double)(end - start)) / CLOCKS_PER_SEC);

    return (i < size);

}

int majorityWorst(int *array, int size, int *majority) {
    int max = 0, i;
    clock_t start = clock();
    for (i = 0; i < size; i++) {
        if (max < array[i]) {
            max = array[i];
        }
    }

    int array_el[max + 1];
    for (i = 0; i <= max; i++) {
        array_el[i] = 0;
    }

    for (i = 0; i <= (size / 2) + 1; i++) {
        if (array_el[array[i]] > 0) {
            continue;
        }
        int j;
        int occurence = 1;
        int flag = 0;
        for (j = i + 1; j <= size; j++) {
            if (occurence >= (size / 2) + 1) {
                *majority = array[i];
                flag = 1;
                break;
            } else if (array[i] == array[j]) {
                occurence++;
            }
        }
        array_el[array[i]] = occurence;
        if (flag) {
            break;
        }
    }

    clock_t end = clock();
    printf("Tempo di esecuzione =  %f secondi \n", ((double)(end - start)) / CLOCKS_PER_SEC);

    return (i <= (size / 2));

}