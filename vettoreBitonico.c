#include<stdio.h>
#include<stdlib.h>

void copyArray(long *array, long *copy, int end) {
    int k;
    for (k = 1; k <= end; k++) {
        array[k] = copy[k];
    }
}

void merge(long *array, int medium, int end) {
    int toStart = medium;
    int toEnd = medium + 1;
    int f = end;
    long copy[end + 2];
    int k = 1;
    while (toStart > 0 || toEnd <= end) {
        if (toStart > 0 && (toEnd > end || array[toStart] >= array[toEnd])) {
            copy[k] = array[toStart];
            toStart--;
        } else {
            copy[k] = array[toEnd];
            toEnd++;
        }
        k++;
    }
    copyArray(array, copy, end);
}

int max(int start, int medium, int end) {
    if (start < medium && medium > end) {
        return medium;
    } else if (start > medium && start > end) {
        return start;
    } else if (end > medium && end > start) {
        return end;
    }
}

int maxPosition(long *array, int start, int medium, int end) {
    if (array[start] < array[medium] && array[medium] > array[end]) {
        return medium;
    } else if (array[start] > array[medium] && array[start] > array[end]) {
        return start;
    } else if (array[end] > array[medium] && array[end] > array[start]) {
        return end;
    }
}

int maxBitonic(long *array, int start, int end) {
    int medium = (start + end) / 2;
    if (end - start == 1 || end - start == 0 || (medium + 1 == end && medium - 1 == start)) {
        return max (array[medium], array[start], array[end]);
    } else {
        if (array[medium] > array[medium + 1] && array[medium] > array[medium - 1]) {
            return array[medium];
        } else if (array[medium] > array[medium + 1]) {
            return maxBitonic(array, start, medium);
        } else {
            return maxBitonic(array, medium, end);
        }
    }
}

void fromBitonicToOrderedArray(long *array, int start, int end, int len) {
    int medium = (start + end) / 2;
    if (end - start == 1 || end - start == 0 || (medium + 1 == end && medium - 1 == start)) {
        merge(array, maxPosition(array, medium, start, end), len);
    } else {
        if (array[medium] > array[medium + 1] && array[medium] > array[medium - 1]) {
            merge(array, medium, len);
        } else if (array[medium] > array[medium + 1]) {
            fromBitonicToOrderedArray(array, start, medium, len);
        } else {
            fromBitonicToOrderedArray(array, medium, end, len);
        }
    }
}

int main (int argc, char **argvs) {
    long array[argc];

    int i = 0;
    for (i = 1; i < argc; i++) {;
        array[i] = atoi(argvs[i]);
    }

    fromBitonicToOrderedArray(array, 1, argc - 1, argc - 1);

    for (i = 1; i < argc; i++) {;
        printf("%ld\t", array[i]);
    }
}