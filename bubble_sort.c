#include<stdio.h>

int main() {

    int array[] = { 6, 5, 4, 6, 7, 1, 11, 12, 15, 9, 3, 2, 1 };
    int i, j;

    for (i = 0; i < 13; i++) {
        for (j = 12; j > i; j--) {
            if (array[j] < array[j - 1]) {
                int temp = array[j];
                array[j] = array[j - 1];
                array[j - 1] = temp;
            }
        }
    }
    
    for (j = 0; j < 13; j++) {
        printf("%d ", array[j]);
    }

    printf("\n");

}