#include<stdio.h>
#include <stdlib.h>

#define MAX_LINE_SIZE 1000   // maximum size of a line of input


int scanArray(int *a) {
    // scan line of text
    char line[MAX_LINE_SIZE];
    scanf("%[^\n]s", line);

    // convert text into array
    int size = 0, offset = 0, numFilled, n;
    do {
        numFilled = sscanf(line + offset, "%d%n", &(a[size]), &n);
        if (numFilled > 0) {
            size++;
            offset += n;
        }
    } while (numFilled > 0);

    return size;
}


void sumInterval(int *array, int size, int s, int *start, int *end);

int main() {

    char line[MAX_LINE_SIZE];
    int array[MAX_LINE_SIZE];
    int size = scanArray(array);
    int numToSearch;
    scanf("%d", &numToSearch);
    
    int start = -1;
    int end = -1;

    sumInterval(array, size, numToSearch, &start, &end);
    printf("%d %d", start, end);
    printf("\n");
}

void sumInterval(int *array, int size, int numToSearch, int *start, int *end) {
    int i, j = 0;
    int sum = array[0];
    int flag = 0;
    for (i = 1; i < size && !flag; i++) {
        printf("%d ", sum);
        if (sum + array[i] > numToSearch) {
            sum -= array[j];
            j++;
            if (array[j] == 0) {
                j++;
            }
            printf("x");
        }
        sum += array[i];
        if (sum == numToSearch && i != j) {
            *start = j;
            *end = i;
            flag = 1;
        }
    }
}