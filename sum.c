#include<stdio.h>
#include<stdlib.h>

int sumInterval(long *array, int size, long s, int *start, int *end);
int binaryResearch(long *array, int posStart, int posEnd, long key);

int main(int argc, char **argvs) {

    int start = 0;
    int end = 0;
    int size = argc - 2;
    int val = atoi(argvs[argc - 1]);
    long array[argc - 2];

    int i = 0;
    for (i = 0; i < argc - 2; i++) {;
        array[i] = atoi(argvs[i + 1]);
    }
    for (i = 0; i < argc - 2; i++) {;
        printf("%ld\t", array[i]);
    }

    if (sumInterval(array, size, val, &start, &end)) {
        printf("%d, %d", start, end);
    } else {
        printf("-1, -1");
    }

    return 0;
}

int sumInterval(long *array, int size, long s, int *start, int *end) {
    int j;
    int flag = 0;
    for (j = 0; j < size - 1 && !flag; j++) {
        int num = s - array[j];
        int pos = binaryResearch(array, j + 1, size, num);
        if (pos != -1) {
            *start = j; *end = pos;
            flag = 1;
        }
    }
    return flag;
}

int binaryResearch(long *array, int posStart, int posEnd, long key) {
    int len = posEnd - posStart + 1;
    if (len < 1) {
        return (array[posStart] == key) ? posStart : -1;
    } else {
        len = (len / 2) + posStart;
        if (array[len] < key) {
            binaryResearch(array, len + 1, posEnd, key);
        } else if (array[len] > key) {
            binaryResearch(array, posStart, len - 1, key);
        } else {
            return len;
        }
    }
}