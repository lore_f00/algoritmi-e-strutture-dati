#include<stdio.h>

void switchValues(int *array, int start, int end) {
    int temp = array[end];
    array[end] = array[start];
    array[start] = temp;
}

void printArray(int *array) {
    for (int i = 1; i <= 5; i++) {
        printf("%d ", array[i]);
    }
}

int main () {
    int array[] = { 0, 5, 4, 3, 2, 8};
    for (int i = 1; i < 5; i++) {
        if (i % 2 == 0) {
            if (array[i + 1] < array[i]) {
                switchValues(array, i, i + 1);
            }
        } else {
            if (array[i + 1] > array[i]) {
                switchValues(array, i, i + 1);
            }
        }
    }

    printArray(array);

    return 0;
}