#include <stdio.h>
#include <math.h>

int majority_candidate(int *v, int dim) {
	int pos_candidate = 0;
	int count = 0;
	for (int i = 1; i < dim; i++) {
		if (v[i] == v[pos_candidate])
			count++;
		else
			count--;
		if (count == -1 && i < dim -1) {
			pos_candidate = i;
			count = 0;
		}
	}
	return v[pos_candidate];
}

int is_majority(int *v, int dim, int el) {
	int count = 0;
	for (int i = 0; i < dim; i++) {
		if (v[i] == el) {
			count++;
		}
	}

	return (count >= (int)ceil(dim/2)) ? 1 : 0;

}

int main()
{
	int v[] = { 1, 1, 1, 4, 5, 6, 1, 7 };
	int len = sizeof(v)/sizeof(v[0]);
	printf("dimensione array: %d\n", len);

	int majorityCandidate = majority_candidate(v, len);
	((is_majority(v, len, majorityCandidate))) ?
		printf("Majority Candidate: %d\n", majorityCandidate)
	:
		printf("Majority Candidate not present!\n");

}